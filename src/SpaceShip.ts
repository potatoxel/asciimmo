//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { PrefabName } from '../client/shared/Prefabs';
import { Vector3 } from '../client/shared/Vector3';
import { ServerGameObject } from './ServerGameObject';
import { TileMapObject } from './TileMapObject';
import { trim_amount } from '../client/shared/Utils';
import { NetworkPlayer } from './NetworkPlayer';
import { DirectionSymbol, direction_symbol_add, direction_symbol_to_number, direction_symbol_to_vector, subtract_direction_symbols } from '../client/shared/DirectionUtils';
import { ServerSerializedGameObject } from './ServerSerializedGameObject';
import { Laser } from './Laser';
import { ITileBehaviour } from './tiles/Tile';
import { ClientHandler } from './ClientHandler';
import { NetworkEntity } from './NetworkEntity';
import { Chest } from './Chest';
import { tileManager } from './tiles/Tiles';
import { Item } from './items/Item';
import { Computer } from './Computer';


class ArrowBehaviour implements ITileBehaviour {

    use(client: ClientHandler, tileMap: TileMapObject, localPosition: Vector3): void {
        
    }

    collide(tileMap: TileMapObject, localPosition: Vector3, collider: ServerGameObject, tileSymbol: string): void {
        
        if(tileMap instanceof SpaceShip) {
            
            let rotationNumber = subtract_direction_symbols(
                tileSymbol as DirectionSymbol, tileMap.direction as DirectionSymbol
                );
                
            tileMap.direction = tileSymbol as DirectionSymbol;
            let dir: Vector3 = direction_symbol_to_vector[tileMap.direction];

            let newPosition = tileMap.position.add(dir.mul(2));
                
            tileMap.rotateAndSetPosition(newPosition, rotationNumber);
        }
    }
}

class ShootingBehaviour implements ITileBehaviour {
    use(client: ClientHandler, tileMap: TileMapObject, localPosition: Vector3): void {
        
    }

    collide(tileMap: TileMapObject, localPosition: Vector3, collider: ServerGameObject, tileSymbol: string): void {
        
        if(tileMap instanceof SpaceShip && collider instanceof NetworkEntity) {
            tileMap.shoot();
        }
    }
    
}

export class SpaceShip extends TileMapObject {

    public direction: DirectionSymbol; 
    private computer = new Computer();

    constructor() {
        super();

        this.direction = "^";
        this.setupWithText(trim_amount(`
  #####  
 #  ^o #
#       #
#       
#<     >#
#       #
#       #
#   v   #
#########`, 1, "left"));

        this.overrideTileBehaviour(["<", "^", ">", "v"], new ArrowBehaviour());
        this.overrideTileBehaviour(["o"], new ShootingBehaviour());
    }

    ready() {
        this.computer.bind("move", 2, (x: number, y: number) => {
            this.rotateAndSetPosition(this.position.add(new Vector3(x, y, 0)), 0);
        });
        this.computer.bind("shoot", 0, () => {
            this.shoot();
        });
        this.computer.execute("name 'spaceship" + this.id);        
    }

    deserialize(data: ServerSerializedGameObject) {
        super.deserialize(data);
        let publicData = data.publicData.data as {direction: DirectionSymbol};
        this.direction = publicData.direction;
    }

    getPublicData() {
        let res = super.getPublicData();
        let data = res.data as {direction: DirectionSymbol};
        data.direction = this.direction;
        return res;
    }

    getPrivateData() {
        return {
            prefab: "spaceship"
        } as {prefab: PrefabName}
    }


    rotateAndSetPosition(newPosition: Vector3, rotationAmount: 0 | 1 | 2 | 3) {
        let colls = this.world.findEntitiesCollidingWith(this);
        this.tilemap.rotateRight(-rotationAmount);
        
        this.rotateAndMovePositionsOfEntities(
            colls,
            (go) => !go.movable,
            rotationAmount,
            newPosition,
            this.tilemap.width, this.tilemap.height
        );
        
        this.position = newPosition;
        this.commitChanges();
        this.emitPosition();
    };

    getComputer() {
        return this.computer;
    }

    shoot() {
        let atPosition = new Vector3(0, 0, 0); //pls do something
        let collisions = this.world.raycast(atPosition.add(this.position), direction_symbol_to_vector[this.direction], 20, this);
        let distance = 20;
        let globalAtPosition = atPosition.add(this.position);

        if (collisions.length > 0) {
            let tilemap = collisions[0].gameObject;
            if(tilemap instanceof TileMapObject) {
                distance = Math.round(collisions[0].position.sub(globalAtPosition).length())-2;
                if(distance < 0) distance = 0;
                const tiles = tilemap.damageTilesAtWorldSpace(collisions[0].position, 2);
                tilemap.commitChanges();

                const items: Array<Item> = [];
                tiles.forEach(tileSymbol => {
                    let tile = tileManager.tilesByChar.get(tileSymbol);
                    if(tile && tile.getItem()) {
                        items.push(tile.getItem());
                    }
                });

                const neighbors = this.get4NeighborsAt(atPosition);
                for(let i = 0; i < neighbors.length; i++) {
                    const neighbor = neighbors[i];
                    if(neighbor instanceof Chest) {
                        items.forEach(item => {
                            neighbor.inventory.addItem(item.id, 1);
                        });
                        break;
                    }
                }
            }
        }

        let direction_vector = direction_symbol_to_vector[this.direction];
        let laser = new Laser(distance, direction_symbol_to_number[this.direction]);
        if(this.direction == ">" || this.direction  == "v") {
            laser.position = globalAtPosition.add(direction_vector.mul(2));
        } else {
            laser.position = globalAtPosition.add(direction_vector.mul(distance+2));
        }
        this.world.addChild(laser);
    }
}
