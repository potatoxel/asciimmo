//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { SpawnGameObjectData } from '../client/shared/SpawnGameObjectData';
import { ServerGameObject } from './ServerGameObject';
import { ChatMessageData } from '../client/shared/ChatMessageData';
import { ClientHandler } from './ClientHandler';
import { Vector2 } from '../client/shared/Vector2';
import { VSTCodeEditorSpawnData } from '../client/shared/VSTCodeEditorSpawnData';
import { VisibilitySetData } from '../client/shared/VisibilitySetData';
import { CodeData } from '../client/shared/CodeData';
import { Signal } from '../client/shared/Signal';

export class VSTCodeEditor extends ServerGameObject {
    shouldBeSerialized = false;
    onSetCode = new Signal<[ClientHandler, string]>();

    constructor() {
        super();

        this.messageHandler.on("setCode", (sender, data: CodeData) => {
            this.onSetCode.emitSignal(sender, data.code);
        });
    }

    getPublicData(): SpawnGameObjectData {
        return {
            id: this.id,
            sprite: " ",
            x: 0,
            y: 0,
            z: 0,
            prefab: "vstCodeEditor",
            data: {
                position: new Vector2(10, 10),
                size: new Vector2(30, 10),
                code: ""
            } as VSTCodeEditorSpawnData
        };
    }

    sendCode(sender: ClientHandler, msg: string) { 
        this.emitTo(sender, 'setCode', {
            code: msg
        } as CodeData);
    }

    setVisiblity(sender: ClientHandler, visibility: boolean) {
        this.emitTo(sender, 'setVisibility', {
            visible: visibility
        } as VisibilitySetData);
    }
}
