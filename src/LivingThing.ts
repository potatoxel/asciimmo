//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion


import { ItemStack } from "../client/shared/Item";
import { Chest } from "./Chest";
import { NetworkEntity } from "./NetworkEntity";

export class LivingThing extends NetworkEntity {
    public health: number = 10;
    protected loot = new Array<ItemStack>();
    
    takeDamage(damage: number) {
        this.health -= damage;
        if(this.health <= 0) {
            this.die();
        }
    }

    die() {
        this.world.queueRemoveChild(this);
        if (this.loot.length > 0) {
            let chest = new Chest();
            chest.position = this.position;
            chest.disappearWhenEmpty = true;
            this.world.addChild(chest);
            this.loot.forEach((value) => {
                chest.inventory.addItem(value.id, value.quantity);
            });
        }
    }
}