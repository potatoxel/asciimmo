//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { SpawnGameObjectData } from '../client/shared/SpawnGameObjectData';
import { ServerGameObject } from './ServerGameObject';
import { ChatMessageData } from '../client/shared/ChatMessageData';
import { ChatBoxSpawnData } from '../client/shared/ChatBoxSpawnData';
import { ClientHandler } from './ClientHandler';
import { Vector2 } from '../client/shared/Vector2';
import { VisibilitySetData } from '../client/shared/VisibilitySetData';
import { Signal } from '../client/shared/Signal';


export class TerminalDisplay extends ServerGameObject {
    shouldBeSerialized = false;
    public onMessage = new Signal<[ClientHandler, string]>();

    constructor() {
        super();

        this.messageHandler.on("message", (sender, data: ChatMessageData) => {
            this.onMessage.emitSignal(sender, data.message);
        });

    }

    getPublicData(): SpawnGameObjectData {
        return {
            id: this.id,
            sprite: " ",
            x: 0,
            y: 0,
            z: 0,
            prefab: "chatBox",
            data: {
                autoSendLoginMessage: false,
                position: new Vector2(10, 10),
                size: new Vector2(30, 10),
                chatKey: "none",
                chatPrefix: "Ter",
                visible: false
            } as ChatBoxSpawnData
        };
    }

    emitMessageTo(sender: ClientHandler, msg: string) { 
        this.emitTo(sender, 'message', {
            message: msg
        } as ChatMessageData);
    }
    
    emitMessage(msg: string) {
        this.emit("message", {
            message: msg
        } as ChatMessageData);
    }

    clear(client: ClientHandler) {
        this.emitTo(client, "clear", {});
    }

    setVisibility(client: ClientHandler, visible: boolean) {
        this.emitTo(client, "setVisibility", {
            visible: visible
        } as VisibilitySetData);
    }
}
