//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { SpawnGameObjectData } from '../client/shared/SpawnGameObjectData';
import { ServerGameObject } from './ServerGameObject';
import { ChatMessageData } from '../client/shared/ChatMessageData';
import { ChatBoxSpawnData } from '../client/shared/ChatBoxSpawnData';
import { chatHandler, ChatHandler } from './commands/ChatHandler';
import { ClientHandler } from './ClientHandler';
import { plsInitializeCommands } from './commands/Commands';
import { Vector2 } from '../client/shared/Vector2';

//TODO: dont do this, this is just to import commands and so
//that the commands get added.
plsInitializeCommands;

export class ChatBox extends ServerGameObject {
    shouldBeSerialized = false;

    constructor() {
        super();

        this.messageHandler.on("message", (sender, data: ChatMessageData) => {
            chatHandler.handleChat(this, sender, data.message);

            if(!data.message.startsWith("/")){
                let message = sender.userInfo.username + ": " + data.message;
                
                this.emit('message', {
                    message: message
                } as ChatMessageData);
            }
        });

    }

    getPublicData(): SpawnGameObjectData {
        return {
            id: this.id,
            sprite: " ",
            x: 0,
            y: 0,
            z: 0,
            prefab: "chatBox",
            data: {
                autoSendLoginMessage: true,
                position: new Vector2(0, 0),
                size: new Vector2(30, 10),
                chatPrefix: "Chat",
                chatKey: ">",
                visible: true
            } as ChatBoxSpawnData
        };
    }

    sendMessage(sender: ClientHandler, msg: string) { 
        this.emitTo(sender, 'message', {
            message: msg
        } as ChatMessageData);
    }
}
