//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { ItemStack } from '../client/shared/Item';
import { PrefabName } from '../client/shared/Prefabs';
import { Chest } from './Chest';
import { NetworkEntity } from './NetworkEntity';
import { ServerSerializedGameObject } from './ServerSerializedGameObject';

//For performance eventually make this into a Tile-Entity.
export class ResourceGenerator extends NetworkEntity {
    movable = false;

    generateItemId: string;
    itemGeneratePeriod: number = 10;
    itemsGeneratedPerPeriod: number = 1;

    constructor() {
        super();
        this.prefab = "entityCharSprite";
        this.sprite = "R";
        
    }

    ready() {
        if(this.generateItemId)
            this.every(this.itemGeneratePeriod, () => {
                var neighbors = this.get4Neighbors();
                neighbors.forEach((value, index, array) => {
                    if (value instanceof Chest) {
                        value.inventory.addItem(
                            this.generateItemId, this.itemsGeneratedPerPeriod
                        );
                    }
                });
            });
    }

    deserialize(data: ServerSerializedGameObject) {
        super.deserialize(data);
        var generateItemId = data.privateData.generateItemId;
        var itemGeneratePeriod = data.privateData.itemGeneratePeriod || 10;
        var itemsGeneratedPerPeriod = data.privateData.itemsGeneratedPerPeriod || 1;
        
        this.generateItemId = generateItemId;
        this.itemGeneratePeriod = itemGeneratePeriod;
        this.itemsGeneratedPerPeriod = itemsGeneratedPerPeriod;
    }

    getPrivateData() {
        return {
            prefab: 'resourceGenerator',
            generateItemId: this.generateItemId,
            period: this.itemGeneratePeriod,
            itemPerPeriod: this.itemsGeneratedPerPeriod
        } as { prefab: PrefabName; };
    }

    update() {
        super.update();

    }

}
