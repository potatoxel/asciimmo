import { PrefabName } from '../client/shared/Prefabs';
import { RecieveItemListData } from '../client/shared/RecieveItemListData';
import { UseItemData } from '../client/shared/UseItemData';
import { Vector2 } from '../client/shared/Vector2';
import { ClientHandler } from './ClientHandler';
import { Inventory } from './Inventory';
import { Assembly1, FunctionBinding } from './languages/assembly1/Assembly1';
import { NetworkEntity } from './NetworkEntity';


export class Computer extends NetworkEntity {
    private language: Assembly1;
    private users = new Set<ClientHandler>();
    private currentScreen = new Array<string>();
    private screenSize = new Vector2(0, 10);
    public static computers = new Map<string, Computer>();
    private code: string = "";
    private onSetCodeListener = (clientHandler: ClientHandler, code: string) => this.onSetCode(clientHandler, code);
    private extraBindings = new Array<{name: string, func: FunctionBinding, argCount: number}>();
    public name: string;
    
    constructor() {
        super();
        this.prefab = "entityCharSprite";
        this.sprite = "C";
        this.movable = true;
        
        this.language = new Assembly1();
        this.makeLanguageBindings(this.language);
        this.language.compile(this.code);
        
    }

    private makeLanguageBindings(language: Assembly1) {
        language.bind("print", 1, (str) => {
            this.computerPrint("" + str);
        });

        language.bind("name", 1, (str) => {
            Computer.computers.set(str, this);
            this.name = str;
        });

        language.bind("clear", 1, (str) => {
            this.computerClear();
        });

        language.bind("rpc", 2, (name, code) => {
            let computer = Computer.computers.get(name);
            let language = new Assembly1();
            computer.makeLanguageBindings(language);
            language.setParsedCode(code);
            while(language.step());
        });

        language.bind("get_OEH", 0, () => {
            let entities = this.world.findEntitiesCollidingWithPoint(this.position);
            for(let i = 0; i < entities.length; i++) {
                let entity = entities[i];
                if (entity != this) {
                    return entity;
                }
            }
            return null;
        });

        language.bind("get_CPU", 1, (entity: NetworkEntity) => {
            return entity.getComputer();
        });

        language.bind("CPU_name", 1, (cpu: Computer) => {
            return cpu.name;
        });

        language.bind("this", 0, () => {
            return this;
        });

        language.bind("set_code", 1, (code: string) => {
            this.code = code;
            this.language.compile(this.code);
        });

        language.bind("get_code", 0, () => {
            return this.code;
        });

        this.extraBindings.forEach(value => {
            language.bind(value.name, value.argCount, value.func);
        });
    }

    bind(name: string, argCount: number, func: FunctionBinding) {
        this.language.bind(name, argCount, func);
        this.extraBindings.push({
            name: name,
            argCount: argCount,
            func: func
        });
    }

    execute(code: string) {
        let language = new Assembly1();
        this.makeLanguageBindings(language);
        language.compile(code);
        while(language.step());
    }

    ready() {
        const terminalDisplay = this.world.server.terminalDisplay;
        
        terminalDisplay.onMessage.connect(
            (clientHandler, message) => this.onMessage(clientHandler, message)
        );
    }

    update() {
        if(this.language) {
            this.language.step();
        }
    }

    private computerPrint(str: string) {
        const terminalDisplay = this.world.server.terminalDisplay;
        this.users.forEach(user => {
            terminalDisplay.emitMessageTo(user, str);
        });
        this.currentScreen.push(str);
        if(this.currentScreen.length > this.screenSize.y) {
            this.currentScreen.splice(0,1);
        }
    }

    private computerClear() {
        this.currentScreen = new Array<string>();
    }

    getPrivateData() {
        return {
            prefab: "computer"
        } as { prefab: PrefabName; };
    }

    use(clientHandler: ClientHandler) {
        this.users.add(clientHandler);
        const terminalDisplay = this.world.server.terminalDisplay;

        terminalDisplay.clear(clientHandler);
        this.currentScreen.forEach(str => {
            terminalDisplay.emitMessageTo(clientHandler, str);
        });

        terminalDisplay.setVisibility(clientHandler, true);
    }

    onMessage(clientHandler: ClientHandler, message: string): void {
        if(!this.users.has(clientHandler)) return;
        const terminalDisplay = this.world.server.terminalDisplay;
        if(message == "exit") {
            terminalDisplay.setVisibility(clientHandler, false);
            this.users.delete(clientHandler);
            this.world.server.vstCodeEditor.onSetCode.disconnect(this.onSetCodeListener);
        } else if(message == "reset") {
            this.language.reset();
        } else if(message == "edit") {
            this.world.server.vstCodeEditor.onSetCode.connect(this.onSetCodeListener);
            this.world.server.vstCodeEditor.sendCode(clientHandler, this.code);
            this.world.server.terminalDisplay.setVisibility(clientHandler, false);
            this.world.server.vstCodeEditor.setVisiblity(clientHandler, true);
        } else {
            
        }
    }

    onSetCode(clientHandler: ClientHandler, code: string): void {
        if(!this.users.has(clientHandler)) return;
        this.code = code;
        this.language.compile(code);
        this.world.server.vstCodeEditor.setVisiblity(clientHandler, false);
        this.world.server.terminalDisplay.setVisibility(clientHandler, true);  
    }

}
