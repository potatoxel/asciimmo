//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion


export type ArgumentType = "integer"  | "float" | "string" | "boolean";

export type ValueValidity = {
    valid: boolean;
    value?: any;
}

export class CommandArgument {
    type: ArgumentType;
    optional: boolean = false;
    name: string;
    //** attributes by default are optional */
    attribute: boolean = false;
    default: any;
    
    public getValue(value: string): ValueValidity {
        if(!value) return {
            valid: true,
            value: this.default
        };
        switch(this.type) {
            case "float":{
                if(Number.parseFloat(value) != null) {
                    return {
                        valid: true,
                        value: Number.parseFloat(value)
                    }
                }
                break;
            }
            case "integer": {
                if(Number.parseInt(value) != null) {
                    return {
                        valid: true,
                        value: Number.parseInt(value)
                    }
                }
                break;
            }
            case "string":{
                return {
                    valid: true,
                    value: value
                }
            }
            case "boolean": {
                return {
                    valid: true,
                    value: value == "true"
                }
            }
        }

        return {
            valid: false
        }
        
        
    }
}
