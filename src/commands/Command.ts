//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion


import { ChatBox } from "../ChatBox";
import { ClientHandler } from "../ClientHandler";
import { CharacterStream } from "../languages/CharacterStream";
import { ArgumentType, CommandArgument } from "./CommandArgument";

export class Command {
    name: string;
    helpText: string;
    arguments = new Array<CommandArgument>();
    attributes = new Map<string, CommandArgument>();
    
    public get requiredParamCount() {
        let res = 0;
        for(let i = 0; i < this.arguments.length; i++) {
            res += this.arguments[i].optional ? 0 : 1;
        }
        return res;
    }

    public use(chatBox: ChatBox, client: ClientHandler, message: string) {
        let parser = new CommandArgumentsParser(message);
        let parsedArgs = parser.parse();

        let numArgumentsGiven = 0;
        for(let i = 0; i < parsedArgs.length; i++) {
            let parsedArg = parsedArgs[i];
            if(parsedArg.type == "constant")
                numArgumentsGiven++;
        }
        if(numArgumentsGiven > this.arguments.length) {
            return {
                error: "More arguments given than needed."
            }
        }

        if(numArgumentsGiven < this.requiredParamCount) {
            return {
                error: "Less arguments given than required."
            }
        }

        let cleaned_args = new Map<string, any>();
        let argument_i = 0;

        for(let i = 0; i < parsedArgs.length; i++) {
            let parsedArg = parsedArgs[i];
            let argument;
            
            if(parsedArg.type == "constant") {
                argument = this.arguments[argument_i];
                argument_i++;
            } else if(parsedArg.type == "attribute") {
                argument = this.attributes.get(parsedArg.name);
                if(!argument) {
                    return {
                        error: `Attribute named ${parsedArg.name} doesnt exist.`
                    }
                }
            }   

            let value = argument.getValue(parsedArg.value);
            if(value.valid) {
                cleaned_args.set(argument.name, value.value);
            } else {
                return {
                    error: `Argument #${i} named ${argument.name} expects ${argument.type} not ${parsedArg.value}.`
                }
            }
        }

        //console.log(cleaned_args);
        this.execute(chatBox, client, cleaned_args);

        return {};
    }

    protected execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) {

    }

    required(name: string, type: ArgumentType) {
        let argument = new CommandArgument();
        argument.optional = false;
        argument.name = name;
        argument.type = type;
        this.arguments.push(argument);
        return this;
    }

    attribute(name: string, type: ArgumentType, aliases: string[] = []) {
        let argument = new CommandArgument();
        argument.optional = true;
        argument.name = name;
        argument.type = type;
        this.attributes.set(name, argument);
        for(let i = 0; i < aliases.length; i++) {
            this.attributes.set(aliases[i], argument);
        }
        return this;
    }

    
}

export class CommandTokenizer {
    str: string;
    charStream: CharacterStream;

    constructor(str: string) {
        this.str = str;
        this.charStream = new CharacterStream(str);
    }

    nextToken(peek = false) {
        if(peek) {
            this.charStream.startPeek();
        }
        let res;
        this.charStream.skipChars(' ');

        if(!this.charStream.isEOF()) {
            let nextChar = this.charStream.nextChar(true);
            if (nextChar == "-") {
                this.charStream.nextChar();
                
                res = {
                    "type": "attribute",
                    "value": this.charStream.nextCharUntil(" \n")[0]
                }
            } else {
                res = {
                    "type": "argument",
                    "value": this.charStream.nextCharUntil(" \n")[0]
                }
            }
        }

        if(peek) {
            this.charStream.endPeek();
        }
        return res;
    }
}

export class CommandArgumentsParser {
    
    tokenizer: CommandTokenizer;

    constructor(str: string) {
        this.tokenizer = new CommandTokenizer(str);
    }

    parse() {
        let res: Array<any> = [];
        while(true) {
            let token = this.tokenizer.nextToken();
            if(token == null) {
                break;
            }
            if(token.type == "attribute") {
                let peekToken = this.tokenizer.nextToken(true);
                let parsed: {type: string, name: string, value: any} = {
                    "type": "attribute",
                    "name": token.value,
                    "value": "true"
                };
                if(peekToken && peekToken.type == "argument") {
                    parsed.value = peekToken.value;
                    this.tokenizer.nextToken();
                }
                res.push(parsed);
            } else if(token.type == "argument") {
                res.push({
                    "type": "constant",
                    "value": token.value
                });
            }
        }
        return res;
    }



}