//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { GameObject } from "../../client/shared/GameObject";
import { Vector3 } from "../../client/shared/Vector3";
import { ChatBox } from "../ChatBox";
import { Chest } from "../Chest";
import { ClientHandler } from "../ClientHandler";
import { Mob } from "../Mob";
import { comparePassword, User, UserModel } from "../models/UserModel";
import { MovingThing } from "../MovingThing";
import { NPC } from "../NPC";
import { ResourceGenerator } from "../ResourceGenerator";
import { ServerGameObject } from "../ServerGameObject";
import { SpaceShip } from "../SpaceShip";
import { TileMapObject } from "../TileMapObject";
import { chatHandler } from "./ChatHandler";
import { Command } from "./Command";

export var plsInitializeCommands = 1;

class ListCommandsCommand extends Command {
    name = "cmds";
    helpText = "Lists all commands";

    constructor() {
        super();

    }
}
chatHandler.addCommand(ListCommandsCommand);

class LoginCommand extends Command {
    name = "login";
    helpText = "Login";

    constructor() {
        super();
        this.required("username", "string").required("password", "string");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) {
        let username = args.get("username");
        let password = args.get("password");

        UserModel.findOne({
            username: username
        }).then((user) => {
            if (user && comparePassword(user, password)) {
                chatBox.sendMessage(client, "sys> logged in!");
                client.login(user);
            } else {
                chatBox.sendMessage(client, "sys> somethin not workin.");
            }
        });
    }
}
chatHandler.addCommand(LoginCommand);

class ServerCreditsCommand extends Command {
    name = "server_credits";
    helpText = "Shows credits";

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) {
        chatBox.sendMessage(client, "sys> Copyright (C) 2021 waleed177");
        chatBox.sendMessage(client, "And a small contribution from");
        chatBox.sendMessage(client, "sys> Copyright (C) 2021 metamuffin");
    }
}
chatHandler.addCommand(ServerCreditsCommand);

class KeyCommand extends Command {
    name = "key";
    helpText = "Password for registeration";

    constructor() {
        super();
        this.required("key", "string");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        let key = args.get("key");
        if (key == client.player.world.server.alphaKey) {
            client.hasKey = true;
        }
    }
}
chatHandler.addCommand(KeyCommand);

class RegisterCommand extends Command {
    name = "register";
    helpText = "Registeration";

    constructor() {
        super();
        this.required("key", "string");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        if(client.hasKey) {
            let username = args.get("username");
            let password = args.get("password");

            UserModel.findOne({
                username: username
            }).then(userFound => {
                if(userFound) {
                    chatBox.sendMessage(client, "sys> y u register with existing username");
                } else {
                    let user = new UserModel({
                        username: username,
                        password: password
                    } as User);
                    user.save();

                    chatBox.sendMessage(client, "sys> goodjob.");
                }
            })
        }
    }
}
chatHandler.addCommand(RegisterCommand);

//
class PlaceNPCCommand extends Command {
    name = "pnpc";
    helpText = "";

    constructor() {
        super();
        this.required("sprite", "string")
            .required("dialogueName", "string");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        var npc = new NPC();
        npc.sprite = args.get("sprite");
        npc.dialogueName = args.get("dialogueName");
        npc.position = client.player.position.clone();
        client.player.world.addChild(npc);
    }
}
chatHandler.addCommand(PlaceNPCCommand);

class RemoveCollisionsCommand extends Command {
    name = "remc";
    helpText = "";

    constructor() {
        super();
        this.attribute("all", "boolean", ["a"])
            .attribute("others", "boolean", ["o"])
            .attribute("nontilemap", "boolean", ["nt"]);
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        let condition: (gameObject: GameObject) => boolean;
        condition = _ => true;
        
        {
            if(args.get("all")) {
                condition = _ => true;
            }
        }

        {
            let old_condition = condition;
            if(args.get("others")) {
                condition = gameObject => old_condition(gameObject) && gameObject != client.player;
            }
        }

        {
            let old_condition = condition;
            if(args.get("nontilemap")) {
                condition = gameObject => old_condition(gameObject) && !(gameObject instanceof TileMapObject);
            }
        }

        client.player.world.findCollisionsWithPoint(client.player.position).forEach(
            (gameObject, index, array) => {
                if (condition(gameObject))
                    client.player.world.removeChild(gameObject as ServerGameObject);
            } 
        );
    }
}
chatHandler.addCommand(RemoveCollisionsCommand);

class SaveCommand extends Command {
    name = "save";
    helpText = "";

    constructor() {
        super();
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        client.player.world.save();
    }
}
chatHandler.addCommand(SaveCommand);

class CreateTilemapCommand extends Command {
    name = "tilemap";
    helpText = "";

    constructor() {
        super();
        this.required("x", "integer")
            .required("y", "integer")
            .required("z", "integer");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        var tilemap = new TileMapObject();
        tilemap.setup(args.get("x"), args.get("y"), args.get("z"));
        tilemap.position = client.player.position;
        client.player.world.addChild(tilemap);
    }
}
chatHandler.addCommand(CreateTilemapCommand);

class LoadTextTileMapCommand extends Command {
    name = "loadtxtmap";
    helpText = "";

    constructor() {
        super();
        this.required("path", "string");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        var tilemap =  client.player.world.convertTextFileToTileMapObject(args.get("path"));
        tilemap.position = client.player.position;
        client.player.world.addChild(tilemap);
    }
}
chatHandler.addCommand(LoadTextTileMapCommand);

class SetSpawnPointHereCommand extends Command {
    name = "setspawnpointhere";
    helpText = "";

    constructor() {
        super();
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        client.player.world.spawnPoint = client.player.position.clone();
    }
}
chatHandler.addCommand(SetSpawnPointHereCommand);

class SpawnMobCommand extends Command {
    name = "spawnmob";
    helpText = "";

    constructor() {
        super();
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        let mob = new Mob();
        mob.position = client.player.position;
        client.player.world.addChild(mob);
    }
}
chatHandler.addCommand(SpawnMobCommand);

class SpawnShipCommand extends Command {
    name = "spawnship";
    helpText = "";

    constructor() {
        super();
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        let thing = new SpaceShip();
        thing.position = client.player.position;
        client.player.world.addChild(thing); 
    }
}
chatHandler.addCommand(SpawnShipCommand);

class SpawnChestCommand extends Command {
    name = "spawnchest";
    helpText = "derwand";

    constructor() {
        super();
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        let thing = new Chest();
        thing.position = client.player.position;
        client.player.world.addChild(thing); 
    }
}
chatHandler.addCommand(SpawnChestCommand);

class SpawnResourceGenerateCommand extends Command {
    name = "spawnres";
    helpText = "";

    constructor() {
        super();
        this.required("itemId", "string")
            .required("period", "integer")
            .required("amount", "integer");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        let thing = new ResourceGenerator();
        thing.position = client.player.position;
        thing.generateItemId = args.get("itemId");
        thing.itemGeneratePeriod = args.get("period");
        thing.itemsGeneratedPerPeriod = args.get("amount");
        client.player.world.addChild(thing); 
    }
}
chatHandler.addCommand(SpawnResourceGenerateCommand);

class SpawnMovingThingCommand extends Command {
    name = "spawnthing";
    helpText = "";

    constructor() {
        super();
        this.required("dirX", "integer")
            .required("dirY", "integer");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        let thing = new MovingThing();
        thing.position = client.player.position;
        thing.velocity = new Vector3(
            args.get("dirX"),
            args.get("dirY"),
            0
        )
        client.player.world.addChild(thing); 
    }
}
chatHandler.addCommand(SpawnMovingThingCommand);

class AddItemCommand extends Command {
    name = "additem";
    helpText = "";

    constructor() {
        super();
        this.required("itemId", "string")
            .required("amount", "integer");
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        client.player.inventory.addItem(
            args.get("itemId"),
            args.get("amount")
        );
    }
}
chatHandler.addCommand(AddItemCommand);

class ClearInventoryCommand extends Command {
    name = "clearinv";
    helpText = "";

    constructor() {
        super();
    }

    execute(chatBox: ChatBox, client: ClientHandler, args: Map<string, any>) { 
        client.player.inventory.clear();
    }
}
chatHandler.addCommand(ClearInventoryCommand);
