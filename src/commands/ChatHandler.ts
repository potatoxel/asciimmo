//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion


import { ChatBox } from "../ChatBox";
import { ClientHandler } from "../ClientHandler";
import { Command } from "./Command";

interface CommandConstructor {
    new(): Command
}

export class ChatHandler {
    private commands: Map<string, Command>;

    constructor() {
        this.commands = new Map<string, Command>();
    }

    addCommand(construct: CommandConstructor) {
        let command = new construct();
        console.log(`Added ${command.name}.`)
        this.commands.set(command.name, command);
    }
    
    handleChat(chatBox: ChatBox, client: ClientHandler, message: string) {
        if(message.startsWith("/")) {
            var args = message.substr(1).split(" ");
            let command = this.commands.get(args[0]);
            if(command) {
                var res = command.use(chatBox, client, message.substr(command.name.length+1).trimLeft());
                if(res.error) {
                    console.log(res.error);
                }
            }
        }
    }
}

export var chatHandler: ChatHandler = new ChatHandler();
