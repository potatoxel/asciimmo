import { CharacterStream } from "../CharacterStream";

export class Assembly1Tokenizer {
    str: string;
    charStream: CharacterStream;

    constructor(str: string) {
        this.str = str;
        this.charStream = new CharacterStream(str);
    }

    nextToken(peek = false) {
        if(peek) {
            this.charStream.startPeek();
        }
        let res;
        this.charStream.skipChars(' ');

        if(!this.charStream.isEOF()) {
            let peekChar = this.charStream.nextChar(true);
            if(peekChar == "\n") {
                this.charStream.nextChar();
                res = {
                    "type": "EOL"
                }
            } else if(peekChar == "{") {
                this.charStream.nextChar();
                res = {
                    "type": "startProg"
                }
            } else if(peekChar == "}") {
                this.charStream.nextChar();
                res = {
                    "type": "endProg"
                }
            } else {
                let [word] = this.charStream.nextCharUntil(" \n", false);
                if (Number.parseInt(word)) {
                    res = {
                        "type": "constant",
                        "value": Number.parseInt(word)
                    }
                } else if (word.startsWith("'")) {
                    res = {
                        "type": "constant",
                        "value": word.substring(1)
                    }
                } else {
                    res = {
                        "type": "identifier",
                        "value": word
                    }
                }
            }
        } else {
            res = {
                "type": "EOF"
            }
        }

        if(peek) {
            this.charStream.endPeek();
        }
        return res;
    }
}

type Assembly1ParsedProgram = ({
    type: "call";
    name: string;
    arguments: {
        type: "variable" | "constant" | "program";
        name?: string;
        value?: number;
    }[];
})[];

export class Assembly1Parser { 
    tokenizer: Assembly1Tokenizer;

    constructor(str: string) {
        this.tokenizer = new Assembly1Tokenizer(str);
    }

    parse() : Assembly1ParsedProgram {
        let res: Array<any> = [];
        while(true) {
            let token = this.tokenizer.nextToken();
            //console.log(token);
            if(token == null || token.type=="EOF" || token.type=="endProg") {
                break;
            }
            if(token.type == "identifier") {
                let args= [];

                while(true) {
                    let argToken = this.tokenizer.nextToken();
                    console.log(argToken);
                    if(!argToken || argToken.type=="EOF" || argToken.type == "EOL") {
                        break;
                    }
                    if(argToken.type == "identifier") {
                        args.push({
                            "type": "variable",
                            "name":  argToken.value
                        });
                    } else if(argToken.type == "constant") {
                        args.push({
                            "type": "constant",
                            "value": argToken.value
                        })
                    } else if(argToken.type == "startProg") {
                        args.push({
                            "type": "program",
                            "value": this.parse()
                        })
                    }
                }

                res.push({
                    "type": "call",
                    "name": token.value,
                    "arguments": args
                })
            }
        }
        return res;
    }

}

/*
{
    let parser = new Assembly1Parser(`print 'wow 
rpc 'test { 
    print 'wow 
} 
`);
    console.log(JSON.stringify(parser.parse()))
} */
/*
{
    let parser = new Assembly1Tokenizer(`print 'wow
rpc 'test { 
    print 'wow }
`);
    console.log("PARSER");
    while(true) {
        let token = parser.nextToken();
        console.log(token);
        if(token.type == 'EOF') {
            break;
        }
    }
}
*/

export type FunctionBinding = (...a: any) => void;

export class Assembly1 {
    private code: string;
    private parsedCode: Assembly1ParsedProgram;
    private programCounter: number = 0;
    private functionBindings = new Map<string, {func: FunctionBinding, argCount: number}>();
    public variables = new Map<string, any>();
    private timeLeftTillNextInstruction: number = 0;
    private labels = new Map<string, number>();

    constructor() {
        this.bind("set", 2, (name, value) => {
            this.variables.set(name,value);
        }); 
        this.bind("add", 3, (dst, src1, src2) => {
            this.variables.set(dst, src1 + src2);
        });
        this.bind("sub", 3, (dst, src1, src2) => {
            this.variables.set(dst, src1 - src2);
        });
        this.bind("mul", 3, (dst, src1, src2) => {
            this.variables.set(dst, src1 * src2);
        });
        this.bind("div", 3, (dst, src1, src2) => {
            this.variables.set(dst, src1 / src2);
        });
        this.bind("sleep", 1, (time: number) => {
            this.timeLeftTillNextInstruction = time;
        });
        this.bind("label", 1, (name: string) => {
            this.labels.set(name, this.programCounter);
        });
        this.bind("goto", 1, (name: string) => {
            this.programCounter = this.labels.get(name);
        });
    }

    compile(code: string) {
        this.code = code;
        this.parsedCode = (new Assembly1Parser(code)).parse();
        //console.log(JSON.stringify(this.parsedCode));
        this.programCounter = 0;
    }

    setParsedCode(code: Assembly1ParsedProgram) {
        this.parsedCode = code;
    }

    reset() {
        this.variables.clear();
        this.programCounter = 0;
    }

    step() {
        if(this.parsedCode 
                && this.programCounter < this.parsedCode.length
                && this.timeLeftTillNextInstruction <= 0) {
            let statementToExecute = this.parsedCode[this.programCounter];
            if(statementToExecute.type == "call") {
                let args = [];
                for(let i = 0; i < statementToExecute.arguments.length; i++) {
                    let arg = statementToExecute.arguments[i];
                    if(arg.type == "constant" || arg.type == "program") {
                        args.push(arg.value);
                    } else if(arg.type == "variable") {
                        args.push(this.variables.get(arg.name));
                    }
                }
                this.variables.set(
                    "ret",
                    this.functionBindings.get(statementToExecute.name).func(...args)
                );
                console.log(this.variables.get("ret"));
            }
            this.programCounter += 1;
            return true;
        }
        this.timeLeftTillNextInstruction -= 1;
        return false;
    }

    bind(name: string, argCount: number, func: FunctionBinding) {
        this.functionBindings.set(name, {
            func: func,
            argCount: argCount
        })
    }
}