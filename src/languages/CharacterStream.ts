
export class CharacterStream {
    str: string;
    location: number = 0;
    peekStack = new Array<number>();
    
    constructor(str: string) {
        this.str = str;
    }
    
    nextChar(peek: boolean = false) {
        let res = this.str[this.location];
        if (!peek)
        this.location++;
        return res;
    }
    
    skipChars(chars: string, peek: boolean = false) {
        let res = "";
        if (peek) {
            this.startPeek();
        }
        while (true) {
            let nextChar = this.nextChar(true);
            if (nextChar == null)
            break;
            if (chars.indexOf(nextChar) == -1)
            break;
            res += nextChar;
            this.nextChar();
        }
        if (peek) {
            this.endPeek();
        }
        return res;
    }
    
    nextCharUntil(chars: string, peek: boolean = false) {
        let res = "";
        if (peek) {
            this.startPeek();
        }
        let nextChar;
        while (true) {
            nextChar = this.nextChar(true);
            if (nextChar == null)
            break;
            if (chars.indexOf(nextChar) >= 0)
            break;
            res += nextChar;
            this.nextChar();
        }
        if (peek) {
            this.endPeek();
        }
        return [res, nextChar];
    }
    
    startPeek() {
        this.peekStack.push(this.location);
    }
    
    endPeek() {
        this.location = this.peekStack.pop();
    }
    
    isEOF() {
        return this.location >= this.str.length;
    }
    
    stepBack(amount: number) {
        this.location -= 1;
    }
}
