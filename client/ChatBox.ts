//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>
    Copyright (C) 2021 metamuffin <muffin@metamuffin.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion

import { ChatMessageData } from "./shared/ChatMessageData.js";
import { keyboard, renderer } from "./Client.js";
import { ClientGameObject } from "./ClientGameObject.js";
import { Vector2 } from "./shared/Vector2.js";
import { SpawnGameObjectData } from "./shared/SpawnGameObjectData.js";
import { ChatBoxSpawnData } from "./shared/ChatBoxSpawnData.js";
import { VisibilitySetData } from "./shared/VisibilitySetData.js";

export class ChatBox extends ClientGameObject {
    private _typingMode: boolean = false;
    private get typingMode(): boolean {
        return this._typingMode;
    }
    private set typingMode(value: boolean) {
        this._typingMode = value;
        if (this.typingMode) {
            keyboard.claimOwnership(this);
        } else {
            keyboard.releaseOwnership(this);
        }
    }
    private currentSendText: string = "";
    private position: Vector2 = new Vector2(0, 0);
    private size: Vector2 = new Vector2(30, 10);
    private messages: Array<string> = new Array<string>();
    private autoSendLoginMessage: boolean = false;
    private chatKey: string = ">";
    private chatPrefix: string = "Chat";
    private visible: boolean = true;

    init(data: SpawnGameObjectData) {
        const spawnData = data.data as ChatBoxSpawnData;
        this.size = spawnData.size;
        this.position = spawnData.position;
        this.autoSendLoginMessage = spawnData.autoSendLoginMessage;
        this.chatKey = spawnData.chatKey;
        this.chatPrefix = spawnData.chatPrefix;
        this.visible = spawnData.visible;
    }

    constructor() {
        super();
        keyboard.addKeyDownListener(this, (ev) => {
            if (ev.key == this.chatKey) {
                this.typingMode = !this.typingMode;
            } else if (ev.key == "/" && !this.typingMode) {
                this.typingMode = true;
                this.currentSendText = "/";
            } else if (this.typingMode) {
                if (ev.key == "Backspace") {
                    this.currentSendText = this.currentSendText.substring(0, this.currentSendText.length - 1);
                } else if (ev.key == "Enter") {
                    if (this.currentSendText == "/client_credits") {
                        this.messages.push("client> Copyright (C) 2021 waleed177");
                    } else if (this.currentSendText == "/logout") {
                        window.localStorage.clear()
                        window.location.reload()
                    } else {
                        if (this.currentSendText.startsWith("/login")) {
                            const [_, username, password] = this.currentSendText.split(" ")
                            window.localStorage.setItem("username", username)
                            window.localStorage.setItem("password", password)
                        }
                        this.emit('message', {
                            message: this.currentSendText
                        } as ChatMessageData);
                    }
                    this.currentSendText = "";

                    if(this.chatKey != "none") {
                        this.typingMode = false;
                    }
                } else if (ev.key.length == 1 && this.currentSendText.length <= this.size.x - 6) {
                    this.currentSendText += ev.key;
                }
            }
            if (this.typingMode) {
                ev.preventDefault();
            }
        });

        this.messageHandler.on("message", (sender, data: ChatMessageData) => {
            this.messages.push(data.message);
            if(this.messages.length > 10) {
                this.messages.splice(0,1);
            }
        });

        this.messageHandler.on("setVisibility", (sender, data: VisibilitySetData) => {
            this.visible = data.visible;
            if(this.chatKey == "none") {
                this.typingMode = data.visible;
            }
        });
        
        this.messageHandler.on("clear", (sender, data) => {
            this.messages = [];
        });
    }

    ready() {
        if(this.autoSendLoginMessage) {
            const stored_username = window.localStorage.getItem("username")
            const stored_password = window.localStorage.getItem("password")
            if (stored_password && stored_username) {
                this.emit("message", { message: `/login ${stored_username} ${stored_password}` } as ChatMessageData)
            }
        }
    }

    update() {

    }

    guiDraw() {
        if(!this.visible) return;
        renderer.fillTilesScreenCoord(
            this.position.x, this.position.y, 0, 
            this.position.x + this.size.x, this.position.y + this.size.y, 0, '-');

        const messageOffset = Math.max(this.messages.length - 10, 0);

        for (let i = 0; i < 10; i++) {
            if (this.messages[messageOffset + i] != null)
                renderer.writeTextScreenCoord(this.position.x, this.position.y + i, 0, this.messages[messageOffset + i]);
        }

        renderer.writeTextScreenCoord(this.position.x, this.position.y + this.size.y, 0,  this.chatPrefix + (this.typingMode ? "> " : "--") + this.currentSendText);
    }
}
