export interface VisibilitySetData {
    visible: boolean;
}