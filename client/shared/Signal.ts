//type SignalListener = (...a: any) => void;
type SignalListener<ArgumentTypes extends any[]> = (...a:ArgumentTypes)=>void;

export class Signal<ArgumentTypes extends any[]> {
    private listeners = new Set<(...a:ArgumentTypes)=>void>();

    public connect(listener: SignalListener<ArgumentTypes>) {
        this.listeners.add(listener);
    }

    public disconnect(listener: SignalListener<ArgumentTypes>) {
        this.listeners.delete(listener);
    }

    public emitSignal(...args : ArgumentTypes) {
        this.listeners.forEach(listener => listener(...args));
    }
}