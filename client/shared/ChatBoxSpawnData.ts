import {Vector2} from "./Vector2.js";

export interface ChatBoxSpawnData {
    chatPrefix: string;
    chatKey: string;
    size: Vector2;
    position: Vector2;
    autoSendLoginMessage: boolean;
    visible: boolean;
};