import { Vector2 } from "./Vector2.js";

export type VSTCodeEditorSpawnData = {
    size: Vector2;
    position: Vector2;
    code: string;
};
