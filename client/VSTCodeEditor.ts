//#region PREAMBLE
/*
    This is an ASCII MMO game.
    Copyright (C) 2021 waleed177 <potatoxel@gmail.com>
    Copyright (C) 2021 metamuffin <muffin@metamuffin.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, version 3 of the
    License only.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
//#endregion
import { keyboard, renderer } from "./Client.js";
import { ClientGameObject } from "./ClientGameObject.js";
import { Vector2 } from "./shared/Vector2.js";
import { SpawnGameObjectData } from "./shared/SpawnGameObjectData.js";
import { VisibilitySetData } from "./shared/VisibilitySetData.js";
import { CodeData } from "./shared/CodeData.js";
import { VSTCodeEditorSpawnData } from "./shared/VSTCodeEditorSpawnData";

export class VSTCodeEditor extends ClientGameObject {
    private _typingMode: boolean = false;
    private get typingMode(): boolean {
        return this._typingMode;
    }
    private set typingMode(value: boolean) {
        this._typingMode = value;
        if (this.typingMode) {
            keyboard.claimOwnership(this);
        } else {
            keyboard.releaseOwnership(this);
        }
    }

    private position: Vector2 = new Vector2(0, 0);
    private size: Vector2 = new Vector2(30, 10);
    private code: Array<string> = new Array<string>();
    private visible: boolean = false;

    private cursorPosition: Vector2 = new Vector2(0, 0);
    private cursorVisible: boolean = true;

    init(data: SpawnGameObjectData) {
        const spawnData = data.data as VSTCodeEditorSpawnData;
        this.size = spawnData.size;
        this.position = spawnData.position;
    }

    ready() {
        this.every(4, () => {
            this.cursorVisible = !this.cursorVisible;
        });
    }

    constructor() {
        super();
        keyboard.addKeyDownListener(this, (ev) => {
            if(!this.typingMode) return;
            let oldPosition = this.cursorPosition.clone();
            if(ev.key == "ArrowUp") {
                this.cursorPosition.addInPlace(new Vector2(0, -1));
            } else if(ev.key == "ArrowDown") {
                this.cursorPosition.addInPlace(new Vector2(0, 1));
            } else if(ev.key == "ArrowLeft") {
                this.cursorPosition.addInPlace(new Vector2(-1, 0));
            } else if(ev.key == "ArrowRight") {
                this.cursorPosition.addInPlace(new Vector2(1, 0));
            } else if(ev.key == "Enter") {
                this.cursorPosition.addInPlace(new Vector2(0, 1));
                if(this.cursorPosition.y > this.code.length) {
                    this.code.push("");
                } else {
                    const line = this.code[this.cursorPosition.y-1];
                    this.code[this.cursorPosition.y-1] = line.substring(0, this.cursorPosition.x);
                    this.code.splice(
                        this.cursorPosition.y, 0, 
                        line.substring(this.cursorPosition.x, line.length)
                    );
                    this.cursorPosition.x = 0;
                }
            } else if(ev.ctrlKey && ev.key.toLowerCase() == "e") {
                this.visible = false;
                this.typingMode = false;
                this.emit("setCode", {
                    code: this.code.join("\n")
                } as CodeData);
            } else if(ev.key == "Backspace") {
                const line = this.code[this.cursorPosition.y];
                if(!(this.cursorPosition.x == 0 && this.cursorPosition.y == 0)){
                    if(this.cursorPosition.x == 0) {
                        this.code[this.cursorPosition.y - 1] += line;
                        this.code.splice(this.cursorPosition.y, 1);
                        this.cursorPosition.addInPlace(new Vector2(0, -1));
                        this.cursorPosition.x = this.code[this.cursorPosition.y].length-line.length;
                    } else {
                        this.code[this.cursorPosition.y] = 
                            line.substring(0, this.cursorPosition.x-1) 
                            + line.substring(this.cursorPosition.x, line.length);
                        this.cursorPosition.addInPlace(new Vector2(-1, 0));
                    }
                }
            } else if (ev.key.length == 1){
                this.cursorPosition.addInPlace(new Vector2(1, 0));
                if(this.cursorPosition.y >= this.code.length) {
                    this.code[this.cursorPosition.y] = "";
                }
                //this.code[this.cursorPosition.y] += ev.key;
                let line = this.code[this.cursorPosition.y];
                this.code[this.cursorPosition.y] = 
                    line.substring(0, this.cursorPosition.x-1)
                    + ev.key 
                    + line.substring(this.cursorPosition.x-1, line.length);
            }

            if(this.cursorPosition.y < this.code.length) {
                if(this.code[this.cursorPosition.y].length < this.cursorPosition.x) {
                    this.cursorPosition.x = this.code[this.cursorPosition.y].length;
                }
            } else {
                this.cursorPosition = oldPosition.clone();
            }

            if(this.cursorPosition.x < 0) {
                this.cursorPosition.x = 0;
            }

            ev.preventDefault();
        });

        this.messageHandler.on("setCode", (sender, data: CodeData) => {
            this.code = data.code.split("\n");
            console.log(this.code);
            console.log(data);
        });
        
        this.messageHandler.on("setVisibility", (sender, data: VisibilitySetData) => {
            this.visible = data.visible;
            this.typingMode = data.visible;
            this.cursorPosition = new Vector2(0, 0);
        });
    }

    guiDraw() {
        if(!this.visible) return;
        renderer.fillTilesScreenCoord(
            this.position.x, this.position.y, 0, 
            this.position.x + this.size.x, this.position.y + this.size.y, 0, '-');

        const messageOffset = Math.max(this.code.length - 10, 0);

        for (let i = 0; i < 10; i++) {
            if (this.code[messageOffset + i] != null){
                renderer.writeTextScreenCoord(
                    this.position.x, this.position.y + i, 0,
                    this.code[messageOffset + i]
                );
            }
        }  
        
        if(this.cursorVisible)
            renderer.setTileScreenCoord(
                this.position.x + this.cursorPosition.x,
                this.position.y + this.cursorPosition.y,
                0,
                '|'
            );
    }
}
